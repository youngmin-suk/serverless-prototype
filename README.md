## 概要 ##
ID発行CSV生成プログラム（ID管理票(Excel)をCSVファイルに変換する）で
作成したCSVファイルをS3にアップロードすることを前提とする。
S3にID発行用CSVファイルが存在するとファイルの内容をEbicaのDBにインポートする。

## 1. serverlessの導入 ##
#### AWS CLIをインストール ####
    brew install awscli

#### AWS CLI 設定 ####
    ~/.aws/credentials
    [default]
    aws_access_key_id=[Access key ID]
    aws_secret_access_key=[Secret access key]
    
    ~/.aws/config
    [default]
    region = ap-northeast-1

#### serverless framework インストール ####
    npm install -g serverless

#### serverless install確認 ####
    which serverless

#### ※serverlessでもできる ####
    serverless config credentials --provider aws --key [Access key ID] --secret [Secret access key]

#### serverless プロジェクト作成 ####
    serverless create --template aws-nodejs --path ebica-create-users

#### serverless(sls) deploy時のエラー ####
    Error --------------------------------------------------
        ServerlessError: ServerlessError: User: arn:aws:iam::158139854972:user/suk
        is not authorized to perform: cloudformation:DescribeStackResources
        on resource: arn:aws:cloudformation:us-east-1:158139854972:stack/serverlesstest-dev/*
        
        For debugging logs, run again after setting the "SLS_DEBUG=*" environment variable.
        
    Get Support --------------------------------------------
        Docs:          docs.serverless.com
        Bugs:          github.com/serverless/serverless/issues
        
        Please report this error. We think it might be a bug.
        
    Your Environment Information -----------------------------
        OS:                 darwin
        Node Version:       7.6.0
        Serverless Version: 1.8.0
* 対策: amazonアカウントに対してCloudFormationとs3の権限を与える。

#### 次のエラ ####
    $ sls deploy
        
    Serverless: Packaging service...
    Serverless: Uploading CloudFormation file to S3...
        
    Serverless Error ---------------------------------------
        
        The specified bucket does not exist
        
    Get Support --------------------------------------------
        Docs:          docs.serverless.com
        Bugs:          github.com/serverless/serverless/issues
        
    Your Environment Information -----------------------------
        OS:                 darwin
        Node Version:       7.6.0
        Serverless Version: 1.8.0

* 対策を探してみたらIAM権限の設定でエラーが出て直らなかったので、AdministratorAccessに変更

#### 設定ファイル（serverless.yml）の地域（region）を修正する。
    provider:
        name: aws
        runtime: nodejs4.3
        stage: beta
        region: ap-northeast-1


## 2. TypeScript導入 ##
#### yarnがインストールされてない時
    brew install yarn

#### yarnのメリット
* インストールが速い
* lockファイルによるバージョンロック

#### 依存物をインストール(yarn or npm)
##### yarn
    $yarn init
    $yarn add --dev serverless serverless-webpack ts-loader tslint typescript webpack

##### npm
	$ npm init
	$ npm install --save-dev serverless-webpack ts-loader tslint typescript webpack

#### serverless関連コマンド
##### デプロイ
	$ serverless deploy
##### 開発環境デプロイ
	serverless deploy
	serverless deploy --stage dev
##### 開発環境動作確認
	serverless invoke --function hello
	serverless invoke --stage dev --function hello
##### 開発環境削除
	serverless remove
	serverless remove --stage dev
##### 検証環境デプロイ
	serverless deploy --stage stg
##### 検証環境動作確認
	serverless invoke --stage stg --function hello
##### 検証環境削除
	serverless remove --stage stg
##### 本番環境デプロイ
	serverless deploy --stage prd
##### 本番環境動作確認
	serverless invoke --stage prd --function hello
##### 本番環境削除
	serverless remove --stage prd

## 3. Unit Test ##
### Serverless Mocha Plugin ###
https://github.com/SC5/serverless-mocha-plugin
  * 選定理由: インストールとテストがシンプル

#### インストール ####
    yarn add --dev serverless-mocha-plugin

#### Creating functions ####
  * TypeScriptで開発するため、今プロジェクトでは使わない
##### 関数（および関連するテスト）は、コマンドを使用して作成できます
    sls create function -f functionName --handler handler

##### 拡張Create functions #####
ハンドラのコードテンプレートとLambda関数のエントリポイントとしてのJavascript関数module.exports.handlerを
使用して、serverless.ymlに新しい関数myFunctionを作成する。
テストテンプレートもtestに作成される。
オプションで、--pathまたは-pスイッチを使用して特定のフォルダにテストを作成できる。

#### Creating tests ####
    sls create test -f functionName
    例）
    sls create test -f hello
mocha-createコマンドを使用して手動で機能を追加することができる。

#### Running tests ####
    sls invoke test [--stage stage] [--region region] [-f function1] [-f function2] [...]
    例）
    sls create test -f hello